Module: Quick Fix

This is a utility module intended to include a collection of scripts useful for
batch processing contents on a Drupal site.

Currenty Utility:
- HTML entity decode node titles of selected content types


Usage:
Once installed, you should find a menu item "Quick Fix" in the admin menu.
Pick the content types that you wish to perform HTML entity decode to the
titles and click the button "Decode the HTML entities".
