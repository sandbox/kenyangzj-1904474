<?php
/**
 * @file
 * Forms functions goes here.
 */

/**
 * Landing page for quick fix module.
 */
function quick_fix_landing() {
  $form = array();

  $form['title_html_decode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node title - HTML entity decode'),
  );

  $node_types = node_type_get_types();
  $content_types = array();
  foreach ($node_types as $nt) {
    $content_types[$nt->type] = $nt->name;
  }

  $form['title_html_decode']['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Content Type',
    '#options' => $content_types,
  );

  $form['title_html_decode']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Decode title HTML entities',
  );
  return $form;
}

/**
 * Landing form submit.
 */
function quick_fix_landing_submit($form, &$form_state) {

  $content_types = $form_state['values']['content_types'];

  $query = db_query(
      "SELECT nid, title FROM {node} WHERE type IN (:content_types)",
      array(':content_types' => $content_types));

  $batch_ops = array();
  while ($row = $query->fetchAssoc()) {
    $batch_ops[] = array(
      'quick_fix_update_node_title',
      array(
        $row['nid'],
        html_entity_decode($row['title'], ENT_QUOTES, 'UTF-8'),
      ),
    );
  }

  if (!empty($batch_ops)) {
    batch_set(
      array('title' => 'Updating node titles.', 'operations' => $batch_ops)
    );
  }
  else {
    drupal_set_message(
      'Selected content type(s) has no nodes.  Operation aborted.',
      'warning');
  }

}
