<?php
/**
 * @file
 * A bunch of helper functions.
 */

/**
 * Load a node object based on the nid and save it with the new title.
 */
function quick_fix_update_node_title($nid = 0, $new_title = '') {
  if ($nid > 0 && $new_title != '') {
    $node = node_load($nid);
    $node->title = $new_title;
    node_save($node);
  }
}
